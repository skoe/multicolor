/*
 * MultiColor - An image manipulation tool for Commodore 8-bit computers'
 *              graphic formats
 *
 * (c) Thomas Giesel
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Thomas Giesel skoe@directbox.com
 */

#include <stdlib.h>

#include "ToolSprayCan.h"
#include "MCDoc.h"
#include "MCApp.h"

ToolSprayCan::ToolSprayCan()
{
}

ToolSprayCan::~ToolSprayCan()
{
}


/*****************************************************************************/
/*
 * Mouse has been moved while the button was kept pressed.
 *
 * Draw a line segment.
 * X and y are bitmap coordinates.
 */
void ToolSprayCan::Timer(int x, int y)
{
	double angle, radius;
	int xOffset, yOffset;
	BitmapBase* pBitmap = m_pDoc->GetBitmap();

	angle  = ((double)(rand() % 10000) / 10000.0) * 2.0 * 3.14;
	radius = ((double)(rand() % 10000) / 10000.0) * 10.0;
	xOffset = (int)(sin(angle) * radius) / pBitmap->GetPixelXFactor();
	yOffset = (int)(cos(angle) * radius) / pBitmap->GetPixelYFactor();
	pBitmap->SetPixel(x + xOffset, y + yOffset,
					  m_nColorSelected, m_drawingMode);

}

/*****************************************************************************/
/*
 * Finish the tool at the given coordinates (i.e. Mouse button up)
 *
 * Remember the current state for undo.
 * X and y are bitmap coordinates.
 */
void ToolSprayCan::End(int x, int y)
{
    m_pDoc->PrepareUndo();
}

/*****************************************************************************/
/*
 * Return the ID of this tool.
 */
int ToolSprayCan::GetToolId()
{
    return MC_ID_TOOL_SPRAY_CAN;
}
